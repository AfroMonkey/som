#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "som.h"

#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QMainWindow>

#define BMUS 3
#define LEARNING_RATE 0.5

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;

    SOM som;
    std::vector<Neuron *> bmus;

    std::vector<std::vector<QGraphicsRectItem *>> grid;
    void prepare_scene();
    void plot_grid();
    void set_color(const size_t x, size_t y, QColor color, bool bmu = false);
    void generalize();
    void fill();
};

#endif // MAINWINDOW_H

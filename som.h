#ifndef SOM_H
#define SOM_H
#include "neuron.h"

#include <array>
#include <functional>

using COORD = std::pair<size_t, size_t>;

#define ROWS 20
#define COLS 20

class SOM {
public:
    std::array<std::array<Neuron, ROWS>, COLS> neurons;

    SOM(std::function<std::vector<COORD>(size_t, size_t)> gni);
    Neuron &get_bmu(const std::array<double, SPECS> &input);
    std::vector<Neuron *> train(const std::vector<std::array<double, SPECS>> &inputs, const size_t bmus, double learning_rate = 0.5);

    static void normalize(std::vector<std::array<double, SPECS>> &inputs);
};

#endif // SOM_H

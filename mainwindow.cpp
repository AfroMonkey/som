#include "mainwindow.h"

#include "dataset.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QRandomGenerator>

std::vector<COORD> gni(size_t x, size_t y) {
    std::vector<COORD> ni;
    ni.push_back(std::make_pair(x, y));
    if (y > 0) {
        ni.push_back(std::make_pair(x, y - 1));
    }
    if (x > 0) {
        ni.push_back(std::make_pair(x - 1, y));
    }
    if (y < ROWS - 1) {
        ni.push_back(std::make_pair(x, y + 1));
    }
    if (x < COLS - 1) {
        ni.push_back(std::make_pair(x + 1, y));
    }
    return ni;
}

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , som(gni) {
    ui->setupUi(this);

    SOM::normalize(training_set);
    bmus = som.train(training_set, BMUS, LEARNING_RATE);
    prepare_scene();
    plot_grid();
    for (auto bmu : bmus) {
        bmu->color = QColor::fromRgb(QRandomGenerator::global()->generate());
    }
    generalize();
    fill();
    for (auto bmu : bmus) {
        //        set_color(bmu->x, bmu->y, bmu->color, true);
    }
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::prepare_scene() {
    scene = new QGraphicsScene(ui->kanbas->geometry());
    scene->setBackgroundBrush(QBrush(Qt::white));
    ui->kanbas->setScene(scene);
    ui->kanbas->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->kanbas->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void MainWindow::plot_grid() {
    while (not grid.empty()) {
        while (not grid.back().empty()) {
            scene->removeItem(grid.back().back());
            grid.back().pop_back();
        }
        grid.pop_back();
    }
    const double CELL_HEIGHT = ui->kanbas->height() / ROWS;
    const double CELL_WIDTH = ui->kanbas->width() / COLS;

    for (size_t y = 0; y < ROWS; ++y) {
        grid.push_back(std::vector<QGraphicsRectItem *>());
        for (size_t x = 0; x < COLS; ++x) {
            grid.back().push_back(scene->addRect(x * CELL_WIDTH, y * CELL_HEIGHT, CELL_WIDTH, CELL_HEIGHT));
        }
    }
}

void MainWindow::set_color(const size_t x, size_t y, QColor color, bool bmu) {
    grid[x][y]->setBrush(color);
    if (bmu) {
        grid[x][y]->setPen(QPen(Qt::red));
    }
}

void MainWindow::generalize() {
    for (auto &row : som.neurons) {
        for (auto &neuron : row) {
            neuron.bmu = bmus[0];
            for (auto bmu : bmus) {
                if (neuron.get_distance(*bmu) < neuron.get_distance(*neuron.bmu)) {
                    neuron.bmu = bmu;
                }
            }
        }
    }

    std::vector<double> maxs;
    for (auto bmu : bmus) {
        maxs.push_back(0);
        for (auto &row : som.neurons) {
            for (auto &neuron : row) {
                auto distance = neuron.get_distance(*neuron.bmu);
                if (neuron.bmu == bmu and distance > maxs.back()) {
                    maxs.back() = distance;
                }
            }
        }
    }

    for (auto &row : som.neurons) {
        for (auto &neuron : row) {
            for (size_t b = 0; b < bmus.size(); ++b) {
                if (neuron.bmu == bmus[b]) {
                    auto red = neuron.bmu->color.red();
                    auto green = neuron.bmu->color.green();
                    auto blue = neuron.bmu->color.blue();
                    double distance = neuron.get_distance(*neuron.bmu);
                    int alpha = static_cast<int>(255 * (1 - (distance / maxs[b])));
                    neuron.color = QColor(red, green, blue, alpha);
                }
            }
        }
    }
}

void MainWindow::fill() {
    for (auto &row : som.neurons) {
        for (auto &neuron : row) {
            set_color(neuron.x, neuron.y, neuron.color);
        }
    }
}

#include "neuron.h"

#include <algorithm>
#include <random>

Neuron::Neuron()
  : bmu(this) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> randDistrib(-1, 1);
    std::generate(weights.begin(), weights.end(), [&]() -> double { return randDistrib(gen); });
}

double Neuron::get_distance(const std::array<double, SPECS> &input) const {
    double sum = 0;
    for (size_t i = 0; i < weights.size(); ++i) {
        sum += (input[i] - weights[i]) * (input[i] - weights[i]);
    }
    return sqrt(sum);
}

double Neuron::get_distance(const Neuron &neuron) const {
    auto dx = x - neuron.x;
    auto dy = y - neuron.y;
    return sqrt(dx * dx + dy * dy);
}

void Neuron::update_weights(const std::array<double, SPECS> &input, const double learning_rate) {
    for (size_t i = 0; i < weights.size(); ++i) {
        weights[i] += learning_rate * (input[i] - weights[i]);
    }
}

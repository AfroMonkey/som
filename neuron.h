#ifndef NEURON_H
#define NEURON_H

#define SPECS 4
#include <QColor>
#include <array>
#include <vector>

class Neuron {
public:
    size_t x, y;
    QColor color;
    std::vector<Neuron *> neighbors;
    std::array<double, SPECS> weights;
    Neuron *bmu;

    Neuron();
    void set(size_t x, size_t y) {
        this->x = x;
        this->y = y;
    }
    double get_distance(const std::array<double, SPECS> &input) const;
    double get_distance(const Neuron &input) const;
    void update_weights(const std::array<double, SPECS> &input, const double learning_rate);
};

#endif // NEURON_H

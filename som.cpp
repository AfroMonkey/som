
#include "som.h"

#include <algorithm>
#include <random>

SOM::SOM(std::function<std::vector<COORD>(size_t, size_t)> gni) {
    for (size_t y = 0; y < ROWS; ++y) {
        for (size_t x = 0; x < COLS; ++x) {
            neurons[x][y].set(x, y);
            for (auto &n : gni(x, y)) {
                neurons[x][y].neighbors.push_back(&neurons[n.first][n.second]);
            }
        }
    }
}

Neuron &SOM::get_bmu(const std::array<double, SPECS> &input) {
    Neuron *min = &neurons[0][0];
    double min_distance = min->get_distance(input);
    for (auto &ROW : neurons) {
        for (auto &neuron : ROW) {
            double distance = neuron.get_distance(input);
            if (distance < min_distance) {
                min = &neuron;
                min_distance = distance;
            }
        }
    }
    return *min;
}

std::vector<Neuron *> SOM::train(const std::vector<std::array<double, SPECS>> &inputs, const size_t bmus_size, double learning_rate) {
    std::vector<Neuron *> bmus;
    while (bmus.size() < bmus_size) {
        bool repeated;
        Neuron *bmu;
        std::array<double, SPECS> input;
        do {
            input = inputs[static_cast<size_t>(std::rand()) % inputs.size()];
            repeated = false;
            bmu = &get_bmu(input);
            for (auto &b : bmus) {
                if (b == bmu) {
                    repeated = true;
                    break;
                }
            }
        } while (repeated);
        bmus.push_back(bmu);
        for (auto neighbor : bmus.back()->neighbors) {
            neighbor->update_weights(input, learning_rate);
        }
    }
    return bmus;
}

void SOM::normalize(std::vector<std::array<double, SPECS>> &inputs) {
    std::vector<double> mins, maxs;
    for (auto &input : inputs) {
        mins.push_back(*std::min_element(input.begin(), input.end()));
        maxs.push_back(*std::max_element(input.begin(), input.end()));
    }
    auto min = *std::min_element(mins.begin(), mins.end());
    auto max = *std::max_element(maxs.begin(), maxs.end());

    for (auto &input : inputs) {
        for (auto &spec : input) {
            spec = (spec - min) / (max - min);
        }
    }
}
